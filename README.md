Weather App Challenge
=====================

My submission to Gumtree's offline (Mobile Software Engineer) test.

Demos
-----
![By city](demos/search_by_city.gif)

![By zip code](demos/search_by_zip_code.gif)

![By the user's location](demos/search_by_your_location.gif)

![Load most recent search](demos/load_most_recent_search.gif)

App features
------------
* Check the weather by city name or zip code
* Check the weather by the user's location
* Most recent search location loads automatically

Implementation notes
--------------------
* View Architecture: MVVM
* Primary language used: Kotlin
* Project uses packaging-by-feature
* Libraries used (some but not all): Dagger2, Retrofit, Gson, Coroutines, Jetpack/AndroidX, Material Components, Glide, JodaTime
* Rotation/configration changes and process death are all handled.
* Error handling is **not** an afterthought.
* App architecture designed with extensibility in mind.
* ViewModel and (data) models have some unit test coverage.
