package au.com.gumtree.weather.common

import com.google.gson.annotations.SerializedName

data class WeatherApiResponse(
    @SerializedName("coord") val coordinate: Coordinate,
    @SerializedName("weather") val weatherConditions: List<Weather>,
    @SerializedName("main") val main: Main,
    @SerializedName("visibility") val visibility: Int,
    @SerializedName("wind") val wind: Wind,
    @SerializedName("clouds") val cloudiness: Cloudiness,
    @SerializedName("dt") val timestamp: Long,
    @SerializedName("sys") val sys: Sys,
    @SerializedName("timezone") val timezone: Int,
    @SerializedName("id") val cityId: Int,
    @SerializedName("name") val cityName: String
) {

    data class Coordinate(
        @SerializedName("lat") val latitude: Double,
        @SerializedName("lon") val longitude: Double
    )

    data class Weather(
        @SerializedName("id") val id: Int,
        @SerializedName("main") val main: String,
        @SerializedName("description") val description: String,
        @SerializedName("icon") val icon: String
    )

    data class Main(
        @SerializedName("temp") val temperature: Double,
        @SerializedName("feels_like") val feelsLike: Double,
        @SerializedName("temp_min") val minTemperature: Double,
        @SerializedName("temp_max") val maxTemperature: Double,
        @SerializedName("pressure") val pressure: Int,
        @SerializedName("humidity") val humidity: Int
    )

    data class Wind(
        @SerializedName("speed") val speed: Double,
        @SerializedName("deg") val degrees: Int
    )

    data class Cloudiness(
        @SerializedName("all") val all: Double
    )

    data class Sys(
        @SerializedName("country") val country: String,
        @SerializedName("sunrise") val sunriseTimestamp: Int,
        @SerializedName("sunset") val sunsetTimestamp: Int
    )
}
