package au.com.gumtree.weather.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import au.com.gumtree.weather.R
import au.com.gumtree.weather.common.CityQuery
import au.com.gumtree.weather.common.RequestState
import au.com.gumtree.weather.extensions.contains
import au.com.gumtree.weather.extensions.remove
import au.com.gumtree.weather.extensions.shortToast
import au.com.gumtree.weather.extensions.show
import com.google.android.gms.common.api.Status
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.model.*
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import kotlinx.android.synthetic.main.fragment_search_by_city.*

class SearchByCityFragment : Fragment(), PlaceSelectionListener {

    private val viewModel by activityViewModels<SearchViewModel>()
    private val autoCompleteFragment by lazy {
        childFragmentManager.findFragmentById(R.id.autocomplete_fragment)
                as AutocompleteSupportFragment
    }
    private var queryText: String = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_search_by_city, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupAutocompleteWidget()
        if (savedInstanceState != null)
            restorePreviousState(savedInstanceState)
    }

    override fun onPlaceSelected(place: Place) {
        val addressComponents = place.addressComponents ?: return
        val city = addressComponents.find(Place.Type.LOCALITY.name) ?: return
        val country = addressComponents.find(Place.Type.COUNTRY.name) ?: return
        val query = CityQuery(city.name, country.shortName ?: country.name)
        viewModel.weatherBy(query).observe(
            viewLifecycleOwner,
            Observer(::render)
        )
        queryText = city.name
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(BUNDLE_VIEW_STATE, viewModel.cachedData)
        outState.putString(BUNDLE_QUERY_TEXT, queryText)
    }

    override fun onError(status: Status) {
        if (status.isCanceled || status.isInterrupted) return
        context?.shortToast(getString(R.string.something_went_wrong))
    }

    private fun restorePreviousState(savedInstanceState: Bundle) {
        val previousState = savedInstanceState.getParcelable<SearchViewState>(BUNDLE_VIEW_STATE)
        val previousSearchText = savedInstanceState.getString(BUNDLE_QUERY_TEXT, "")
        if (previousState != null) {
            render(previousState)
            autoCompleteFragment.setText(previousSearchText)
            queryText = previousSearchText
        }
    }

    private fun render(viewState: SearchViewState) {
        when (viewState.requestState) {
            RequestState.INITIAL -> {
                searchLoading.remove()
                searchResult.remove()
            }
            RequestState.LOADING -> {
                searchResult.remove()
                searchLoading.show()
            }
            RequestState.SUCCESS -> {
                searchLoading.remove()
                searchResult.show()
                searchResult.bind(viewState.data!!)
                autoCompleteFragment.setText(queryText)
            }
            RequestState.ERROR -> {
                searchLoading.remove()
                searchResult.remove()
                context?.shortToast(getString(R.string.something_went_wrong))
            }
        }
    }

    private fun AddressComponents.find(typeName: String): AddressComponent? {
        return asList().find { component ->
            component.types.contains(typeName, true)
        }
    }

    private fun setupAutocompleteWidget() {
        val boundingRectAus = RectangularBounds.newInstance(
            LatLng(-43.63, 113.34),
            LatLng(-10.67, 153.57)
        )
        autoCompleteFragment.apply {
            setTypeFilter(TypeFilter.CITIES)
            setLocationBias(boundingRectAus)
            setPlaceFields(listOf(Place.Field.ADDRESS_COMPONENTS))
            setOnPlaceSelectedListener(this@SearchByCityFragment)
        }
        autoCompleteFragment.setHint(getString(R.string.check_the_weather_at))
    }

    companion object {
        const val BUNDLE_VIEW_STATE = "BUNDLE_VIEW_STATE"
        const val BUNDLE_QUERY_TEXT = "BUNDLE_QUERY_TEXT"
    }
}
