package au.com.gumtree.weather.di

import android.content.Context
import android.content.SharedPreferences
import au.com.gumtree.weather.common.WeatherApi
import au.com.gumtree.weather.search.*
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.Dispatchers
import javax.inject.Singleton

@Module
object RepositoryModule {

    @Singleton
    @JvmStatic
    @Provides
    fun provideWeatherRepository(api: WeatherApi): WeatherRepository = OnDemandRepository(api)

    @Singleton
    @JvmStatic
    @Provides
    fun provideCountriesRepository(
        appContext: Context,
        gson: Gson
    ): CountriesRepository = DefaultCountriesRepository(appContext, gson, Dispatchers.IO)

    @Singleton
    @JvmStatic
    @Provides
    fun provideRecentSearchesRepository(
        sharedPreferences: SharedPreferences,
        gson: Gson
    ): RecentSearchesRepository = LastSearchRepository(sharedPreferences, gson)
}