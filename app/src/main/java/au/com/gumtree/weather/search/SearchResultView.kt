package au.com.gumtree.weather.search

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import au.com.gumtree.weather.R
import au.com.gumtree.weather.common.WeatherData
import au.com.gumtree.weather.extensions.getColorCompat
import com.bumptech.glide.Glide
import com.google.android.material.card.MaterialCardView
import kotlinx.android.synthetic.main.layout_search_result.view.*

class SearchResultView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null
) : MaterialCardView(context, attrs) {

    init {
        preventCornerOverlap = true
        radius = context.resources.getDimensionPixelSize(R.dimen.radius_xxs).toFloat()
        elevation = context.resources.getDimensionPixelSize(R.dimen.card_elevation).toFloat()
        strokeColor = context.getColorCompat(R.color.colorSecondaryVariant)
        strokeWidth = context.resources.getDimensionPixelSize(R.dimen.card_stroke_width)
        LayoutInflater.from(context).inflate(R.layout.layout_search_result, this)
    }

    fun bind(data: WeatherData) {
        currentLocation.text = data.location
        timestamp.text = context.getString(R.string.as_of_timestamp, data.timestamp)
        weatherDescription.text = data.description
        temperatureValue.text = data.temperature
        feelsLike.text = context.getString(R.string.feels_like, data.feelsLike)
        humidityValue.text = data.humidity
        windSpeedValue.text = data.windSpeed
        loadGraphic(data.iconUrl)
    }

    private fun loadGraphic(url: String) {
        Glide.with(context)
            .load(url)
            .fitCenter()
            .into(weatherGraphic)
    }
}
