package au.com.gumtree.weather.di

import au.com.gumtree.weather.search.SearchActivity
import dagger.Subcomponent

@ActivityScope
@Subcomponent(
    modules = [
        SearchViewModelModule::class
    ]
)
interface SearchComponent {

    @Subcomponent.Factory
    interface Factory {
        fun create(): SearchComponent
    }

    fun inject(activity: SearchActivity)
}
