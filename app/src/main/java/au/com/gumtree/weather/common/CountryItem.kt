package au.com.gumtree.weather.common

import com.google.gson.annotations.SerializedName

data class CountryItem(
    @SerializedName("code")
    val code: String,

    @SerializedName("name")
    val name: String
)