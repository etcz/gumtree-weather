package au.com.gumtree.weather.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import au.com.gumtree.weather.R
import au.com.gumtree.weather.common.WeatherData
import kotlinx.android.synthetic.main.fragment_search_most_recent.*

class LastSearchFragment : Fragment() {

    private val weatherData by lazy {
        requireArguments().getParcelable<WeatherData>(ARGUMENT_DATA)!!
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_search_most_recent, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recentSearchResult.bind(weatherData)
    }

    companion object {
        const val ARGUMENT_DATA = "ARGUMENT_DATA"

        fun newInstance(data: WeatherData): LastSearchFragment {
            val bundle = Bundle()
            bundle.putParcelable(ARGUMENT_DATA, data)
            val fragment = LastSearchFragment()
            fragment.arguments = bundle
            return fragment
        }
    }
}