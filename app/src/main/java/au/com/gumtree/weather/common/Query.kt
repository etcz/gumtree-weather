package au.com.gumtree.weather.common

interface Query {
    fun toMap(): Map<String, String>
}

data class CityQuery(
    val name: String,
    val countryCode: String
) : Query {
    override fun toMap(): Map<String, String> =
        hashMapOf("q" to "$name,${countryCode.toLowerCase()}")
}

data class ZipCodeQuery(
    val code: String,
    val countryCode: String
) : Query {
    override fun toMap(): Map<String, String> =
        hashMapOf("zip" to "$code,${countryCode.toLowerCase()}")
}

data class CoordinateQuery(
    val latitude: Double,
    val longitude: Double
) : Query {
    override fun toMap(): Map<String, String> =
        hashMapOf("lat" to latitude.toString(), "lon" to longitude.toString())
}
