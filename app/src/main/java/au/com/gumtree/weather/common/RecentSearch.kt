package au.com.gumtree.weather.common

import com.google.gson.annotations.SerializedName

data class RecentSearch(
    @SerializedName("data")
    val data: WeatherData,

    @SerializedName("timestamp")
    val timestamp: Long
)