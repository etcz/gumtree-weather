package au.com.gumtree.weather.search

import au.com.gumtree.weather.common.CountryItem

interface CountriesRepository {
    suspend fun all(): List<CountryItem>
}