package au.com.gumtree.weather.search

import au.com.gumtree.weather.common.RecentSearch
import au.com.gumtree.weather.common.WeatherData

interface RecentSearchesRepository {
    suspend fun mostRecent(): RecentSearch?
    suspend fun save(data: WeatherData, timestamp: Long)
}