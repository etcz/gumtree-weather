package au.com.gumtree.weather.search

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import au.com.gumtree.weather.common.Query
import au.com.gumtree.weather.common.WeatherData
import javax.inject.Inject

class SearchViewModel @Inject constructor(
    private val weatherRepo: WeatherRepository,
    private val countriesRepo: CountriesRepository,
    private val recentSearchesRepo: RecentSearchesRepository
) : ViewModel() {

    var cachedData = SearchViewState()

    fun weatherBy(query: Query) = liveData {
        val loading = SearchViewState.loading()
        cachedData = loading
        emit(loading)
        try {
            val apiResult = weatherRepo.fetchForecast(query)
            val data = WeatherData.from(apiResult)
            val now = System.currentTimeMillis()
            recentSearchesRepo.save(data, now)
            val success = SearchViewState.success(data)
            cachedData = success
            emit(success)
        } catch (e: Exception) {
            val error = SearchViewState.error(e)
            cachedData = error
            emit(error)
        }
    }

    fun allCountries() = liveData {
        emit(countriesRepo.all())
    }

    fun lastSearched() = liveData {
        emit(recentSearchesRepo.mostRecent())
    }
}