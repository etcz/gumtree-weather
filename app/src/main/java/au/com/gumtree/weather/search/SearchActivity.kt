package au.com.gumtree.weather.search

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.os.Looper
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import au.com.gumtree.weather.BaseActivity
import au.com.gumtree.weather.R
import au.com.gumtree.weather.common.REQUEST_LOCATION_PERMISSION_CODE
import au.com.gumtree.weather.common.RecentSearch
import au.com.gumtree.weather.extensions.replaceFragment
import com.google.android.gms.location.*
import com.google.android.material.navigation.NavigationView
import pub.devrel.easypermissions.AppSettingsDialog
import pub.devrel.easypermissions.EasyPermissions

class SearchActivity : BaseActivity(),
    NavigationView.OnNavigationItemSelectedListener,
    EasyPermissions.PermissionCallbacks {

    private val viewModel by viewModels<SearchViewModel>()
    private val locationPermissions = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION)

    private lateinit var drawerLayout: DrawerLayout
    private lateinit var drawerToggle: ActionBarDrawerToggle
    private lateinit var navigationView: NavigationView
    private lateinit var locationProviderClient: FusedLocationProviderClient
    private lateinit var locationRequest: LocationRequest
    private lateinit var locationCallback: LocationCallback

    override fun injectDependencies() {
        appComponent.searchSubComponent()
            .create()
            .inject(this)
    }

    override fun getDefaultViewModelProviderFactory() = viewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        setupNavigation()
        setupLocationComponents()
        if (savedInstanceState == null) {
            viewModel.lastSearched().observe(
                this,
                Observer(::showInitialView)
            )
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        drawerLayout.closeDrawers()
        when (item.itemId) {
            R.id.byCity -> {
                showInDetailView(SearchByCityFragment(), FRAG_BY_CITY_TAG)
            }
            R.id.byZipCode -> {
                showInDetailView(SearchByZipCodeFragment(), FRAG_BY_ZIP_CODE_TAG)
            }
            R.id.yourLocation -> {
                requestPermissions()
            }
        }
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE) {
            if (EasyPermissions.hasPermissions(this, *locationPermissions)) {
                requestLocationUpdate()
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        requestLocationUpdate()
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        // Check whether the user denied any permissions and checked "Don't ask again."
        // This will display a dialog directing them to enable the permission in app settings.
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            AppSettingsDialog.Builder(this)
                .setRationale(R.string.permissions_rationale)
                .setPositiveButton(R.string.app_settings)
                .build()
                .show()
        }
    }

    private fun setupNavigation() {
        drawerLayout = findViewById(R.id.mainDrawerLayout)
        drawerToggle = ActionBarDrawerToggle(this, drawerLayout,
            R.string.open,
            R.string.close
        )
        drawerLayout.addDrawerListener(drawerToggle)
        drawerToggle.syncState()
        navigationView = findViewById(R.id.navigationView)
        navigationView.setNavigationItemSelectedListener(this)
    }

    private fun setupLocationComponents() {
        locationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        locationRequest = LocationRequest().apply {
            interval = 0
            fastestInterval = 0
            maxWaitTime = 0
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }
        locationCallback = OneTimeCallback()
    }

    private fun showInitialView(recentSearch: RecentSearch?) {
        if (recentSearch != null) {
            showInDetailView(LastSearchFragment.newInstance(recentSearch.data), FRAG_LAST_SEARCH)
        } else {
            showInDetailView(SearchByCityFragment(), FRAG_BY_CITY_TAG)
        }
    }

    private fun showInDetailView(fragment: Fragment, tag: String) {
        replaceFragment(fragment, R.id.fragmentContainer, tag)
    }

    private fun requestPermissions() {
        if (EasyPermissions.hasPermissions(this, *locationPermissions)) {
            requestLocationUpdate()
        } else {
            EasyPermissions.requestPermissions(
                this,
                getString(R.string.permissions_rationale),
                REQUEST_LOCATION_PERMISSION_CODE,
                *locationPermissions
            )
        }
    }

    private fun requestLocationUpdate() {
        locationProviderClient.requestLocationUpdates(
            locationRequest,
            locationCallback,
            Looper.myLooper()
        )
    }

    private inner class OneTimeCallback : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            super.onLocationResult(locationResult)
            val lastLoc = locationResult.lastLocation
            val fragment = SearchYourLocationFragment.newInstance(lastLoc)
            showInDetailView(fragment, FRAG_YOUR_LOCATION_TAG)
            locationProviderClient.removeLocationUpdates(this)
        }
    }

    companion object {
        private const val FRAG_BY_CITY_TAG = "BY_CITY"
        private const val FRAG_BY_ZIP_CODE_TAG = "BY_ZIP_CODE"
        private const val FRAG_YOUR_LOCATION_TAG = "YOUR_LOCATION"
        private const val FRAG_LAST_SEARCH = "LAST_SEARCH"
    }
}
