package au.com.gumtree.weather.di

import dagger.Module

@Module(
    subcomponents = [
        SearchComponent::class
    ]
)
class SubcomponentsModule
