package au.com.gumtree.weather.extensions

fun List<String>.contains(other: String, ignoreCase: Boolean = false): Boolean {
    return if (ignoreCase) {
        val allElements = map { it.toLowerCase() }
        allElements.contains(other.toLowerCase())
    } else {
        contains(other)
    }
}