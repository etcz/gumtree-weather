package au.com.gumtree.weather.search

import android.content.Context
import au.com.gumtree.weather.common.COUNTRIES_LIST_FILE
import au.com.gumtree.weather.common.CountryItem
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class DefaultCountriesRepository @Inject constructor(
    private val appContext: Context,
    private val gson: Gson,
    private val ioDispatcher: CoroutineDispatcher
) : CountriesRepository {

    @Suppress("BlockingMethodInNonBlockingContext")
    override suspend fun all(): List<CountryItem> {
        return withContext(ioDispatcher) {
            val jsonString = appContext.assets.open(COUNTRIES_LIST_FILE)
                .bufferedReader()
                .use { it.readText() }
            val itemType = object : TypeToken<List<CountryItem>>() {}.type
            gson.fromJson<List<CountryItem>>(jsonString, itemType)
        }
    }
}