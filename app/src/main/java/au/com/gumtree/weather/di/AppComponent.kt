package au.com.gumtree.weather.di

import android.content.Context
import au.com.gumtree.weather.WeatherApp
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        NetworkModule::class,
        ViewModelFactoryModule::class,
        RepositoryModule::class,
        SubcomponentsModule::class
    ]
)
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun appContext(appContext: Context): Builder
        fun build(): AppComponent
    }

    fun searchSubComponent(): SearchComponent.Factory
    fun inject(app: WeatherApp)
}
