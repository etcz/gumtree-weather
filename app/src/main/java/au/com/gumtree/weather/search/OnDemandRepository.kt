package au.com.gumtree.weather.search

import au.com.gumtree.weather.common.Query
import au.com.gumtree.weather.common.WeatherApi
import au.com.gumtree.weather.common.WeatherApiResponse
import javax.inject.Inject

class OnDemandRepository @Inject constructor(
    private val api: WeatherApi
) : WeatherRepository {

    override suspend fun fetchForecast(query: Query): WeatherApiResponse {
        return api.fetchForecast(query.toMap())
    }
}