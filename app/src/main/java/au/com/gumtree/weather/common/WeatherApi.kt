package au.com.gumtree.weather.common

import au.com.gumtree.weather.BuildConfig
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.QueryMap

interface WeatherApi {

    @GET("$API_VERSION/weather")
    suspend fun fetchForecast(
        @QueryMap searchTerms: Map<String, String>,
        @Query("appid") apiKey: String = BuildConfig.WEATHER_API_KEY,
        @Query("units") units: String = MEASUREMENT_UNIT
    ): WeatherApiResponse

    companion object {
        const val API_VERSION = "2.5"
    }
}