package au.com.gumtree.weather.di

import androidx.lifecycle.ViewModelProvider
import au.com.gumtree.weather.ViewModelFactory
import dagger.Binds
import dagger.Module

@Module
abstract class ViewModelFactoryModule {

    @Binds
    abstract fun bindFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}