package au.com.gumtree.weather.common

import android.os.Parcelable
import au.com.gumtree.weather.BuildConfig
import kotlinx.android.parcel.Parcelize
import org.joda.time.DateTimeZone
import org.joda.time.Instant
import org.joda.time.format.DateTimeFormat
import java.net.MalformedURLException
import java.net.URISyntaxException
import java.net.URL

@Parcelize
data class WeatherData(
    val location: String,
    val timestamp: String,
    val description: String,
    val iconUrl: String,
    val temperature: String,
    val feelsLike: String,
    val humidity: String,
    val windSpeed: String
) : Parcelable {

    companion object {
        private fun weatherIconUrl(weather: WeatherApiResponse.Weather): String {
            return try {
                val filename = "${weather.icon}@4x.png"
                val url = URL(BuildConfig.WEATHER_ICONS_ENDPOINT + filename)
                url.toURI().toString()
            } catch (e: MalformedURLException) {
                ""
            } catch (e: URISyntaxException) {
                ""
            }
        }

        private fun formattedDescription(weather: WeatherApiResponse.Weather): String =
            weather.description.capitalize()

        private fun localizedTimestamp(apiResponse: WeatherApiResponse): String {
            val tsMilli = apiResponse.timestamp * 1000
            val tzOffsetMilli = apiResponse.timezone * 1000
            val localTz = DateTimeZone.forOffsetMillis(tzOffsetMilli)
            val dt = Instant.ofEpochMilli(tsMilli).toDateTime()
            val fmt = DateTimeFormat.forPattern("h:mm a zzzz").withZone(localTz)
            return fmt.print(dt)
        }

        // TODO: Provide option to switch measurement system (e.g. metric, imperial)
        private fun appendUnit(temperature: Double): String =
            "${temperature.toInt()}$DEG_CELSIUS"

        fun from(apiResponse: WeatherApiResponse): WeatherData {
            return WeatherData(
                location = "${apiResponse.cityName}, ${apiResponse.sys.country}",
                timestamp = localizedTimestamp(apiResponse),
                description = formattedDescription(apiResponse.weatherConditions.first()),
                iconUrl = weatherIconUrl(apiResponse.weatherConditions.first()),
                temperature = appendUnit(apiResponse.main.temperature),
                feelsLike = appendUnit(apiResponse.main.feelsLike),
                humidity = "${apiResponse.main.humidity}%",
                windSpeed = "${apiResponse.wind.speed} $METERS_PER_SECOND"
            )
        }
    }
}