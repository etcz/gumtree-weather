package au.com.gumtree.weather.common

const val REQUEST_LOCATION_PERMISSION_CODE = 9000
const val COUNTRIES_LIST_FILE = "countries.json"
const val MEASUREMENT_UNIT = "metric"
const val DEG_CELSIUS = "\u2103"
const val METERS_PER_SECOND = "m/s"
