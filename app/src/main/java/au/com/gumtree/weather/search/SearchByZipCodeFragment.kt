package au.com.gumtree.weather.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import au.com.gumtree.weather.R
import au.com.gumtree.weather.common.CountryItem
import au.com.gumtree.weather.common.RequestState
import au.com.gumtree.weather.common.ZipCodeQuery
import au.com.gumtree.weather.extensions.hideSoftKeyboard
import au.com.gumtree.weather.extensions.remove
import au.com.gumtree.weather.extensions.shortToast
import au.com.gumtree.weather.extensions.show
import kotlinx.android.synthetic.main.fragment_search_by_zip_code.*

class SearchByZipCodeFragment : Fragment(), View.OnClickListener {

    private val viewModel by activityViewModels<SearchViewModel>()
    private var countriesMap = hashMapOf<String, String>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_search_by_zip_code, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.allCountries().observe(
            viewLifecycleOwner,
            Observer { countryItems ->
                setAutocompleteData(countryItems)
                setupLookupMap(countryItems)
            }
        )
        searchSubmit.setOnClickListener(this)
        val previousState = savedInstanceState?.getParcelable<SearchViewState>(BUNDLE_VIEW_STATE)
        if (previousState != null)
            render(previousState)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(BUNDLE_VIEW_STATE, viewModel.cachedData)
    }

    override fun onClick(view: View) {
        if (view !== searchSubmit) return
        mainLayout.requestFocus()
        hideSoftKeyboard()
        val selectedCountry = countryInput.text.toString()
        val countryCode = countriesMap[selectedCountry] ?: return
        val zipCode = zipCodeInput.text.toString()
        val query = ZipCodeQuery(zipCode, countryCode)
        viewModel.weatherBy(query).observe(
            viewLifecycleOwner,
            Observer(::render)
        )
    }

    private fun setAutocompleteData(countries: List<CountryItem>) {
        val adapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_dropdown_item_1line,
            countries.map { it.name }
        )
        countryInput.setAdapter(adapter)
        countryInput.threshold = 1
    }

    private fun setupLookupMap(countries: List<CountryItem>) {
        countriesMap.clear()
        countries.forEach {
            countriesMap[it.name] = it.code
        }
    }

    private fun render(viewState: SearchViewState) {
        when (viewState.requestState) {
            RequestState.INITIAL -> {
                searchLoading.remove()
                searchResult.remove()
            }
            RequestState.LOADING -> {
                searchResult.remove()
                searchLoading.show()
            }
            RequestState.SUCCESS -> {
                searchLoading.remove()
                searchResult.show()
                searchResult.bind(viewState.data!!)
            }
            RequestState.ERROR -> {
                searchLoading.remove()
                searchResult.remove()
                context?.shortToast(getString(R.string.something_went_wrong))
            }
        }
    }

    companion object {
        const val BUNDLE_VIEW_STATE = "BUNDLE_VIEW_STATE"
    }
}