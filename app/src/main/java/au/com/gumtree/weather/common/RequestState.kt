package au.com.gumtree.weather.common

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
enum class RequestState : Parcelable {
    INITIAL,
    LOADING,
    SUCCESS,
    ERROR
}

