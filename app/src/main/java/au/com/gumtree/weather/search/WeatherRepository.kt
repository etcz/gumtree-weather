package au.com.gumtree.weather.search

import au.com.gumtree.weather.common.Query
import au.com.gumtree.weather.common.WeatherApiResponse

interface WeatherRepository {
    suspend fun fetchForecast(query: Query): WeatherApiResponse
}
