package au.com.gumtree.weather.extensions

import android.app.Activity
import android.content.Context
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.annotation.IdRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction

@ColorInt
fun Context.getColorCompat(@ColorRes colorRes: Int): Int {
    return ContextCompat.getColor(this, colorRes)
}

fun Context.shortToast(text: String) {
    Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
}

inline fun FragmentManager.transact(actions: FragmentTransaction.() -> FragmentTransaction) {
    beginTransaction()
        .actions()
        .commit()
}

fun FragmentActivity.replaceFragment(fragment: Fragment, @IdRes containerId: Int, tag: String?) {
    val previous: Fragment? = supportFragmentManager.findFragmentByTag(tag)
    if (previous == null) {
        supportFragmentManager.transact { replace(containerId, fragment, tag) }
    }
}

fun FragmentActivity.hideSoftKeyboard() {
    currentFocus?.let {
        val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm?.hideSoftInputFromWindow(it.windowToken, 0)
    }
}

fun Fragment.hideSoftKeyboard() {
    (activity as FragmentActivity).hideSoftKeyboard()
}
