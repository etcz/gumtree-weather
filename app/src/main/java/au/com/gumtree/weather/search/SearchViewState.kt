package au.com.gumtree.weather.search

import android.os.Parcelable
import au.com.gumtree.weather.common.RequestState
import au.com.gumtree.weather.common.WeatherData
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SearchViewState(
    val data: WeatherData? = null,
    val error: Throwable? = null,
    val requestState: RequestState = RequestState.INITIAL
) : Parcelable {

    companion object {
        fun loading() =
            SearchViewState(requestState = RequestState.LOADING)

        fun success(data: WeatherData) =
            SearchViewState(data = data, requestState = RequestState.SUCCESS)

        fun error(t: Throwable) =
            SearchViewState(error = t, requestState = RequestState.ERROR)
    }
}
