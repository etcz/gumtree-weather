package au.com.gumtree.weather

import android.app.Application
import au.com.gumtree.weather.di.AppComponent
import au.com.gumtree.weather.di.DaggerAppComponent
import com.google.android.libraries.places.api.Places
import net.danlew.android.joda.JodaTimeAndroid

class WeatherApp : Application() {

    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        Places.initialize(this, BuildConfig.PLACES_API_KEY)
        JodaTimeAndroid.init(this)
        appComponent = DaggerAppComponent.builder()
            .appContext(this)
            .build()
    }
}
