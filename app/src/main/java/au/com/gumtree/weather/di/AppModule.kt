package au.com.gumtree.weather.di

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object AppModule {

    @Singleton
    @JvmStatic
    @Provides
    fun provideGson(): Gson = Gson()

    @Singleton
    @JvmStatic
    @Provides
    fun provideSharedPreferences(appContext: Context): SharedPreferences =
        appContext.getSharedPreferences("prefs", Context.MODE_PRIVATE)
}