package au.com.gumtree.weather.search

import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import au.com.gumtree.weather.R
import au.com.gumtree.weather.common.CoordinateQuery
import au.com.gumtree.weather.common.RequestState
import au.com.gumtree.weather.extensions.remove
import au.com.gumtree.weather.extensions.shortToast
import au.com.gumtree.weather.extensions.show
import kotlinx.android.synthetic.main.fragment_search_by_city.*

class SearchYourLocationFragment : Fragment() {

    private val viewModel by activityViewModels<SearchViewModel>()
    private val locationToSearch by lazy {
        requireArguments().getParcelable<Location>(ARGUMENT_LOCATION)!!
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_search_your_location, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val previousState = savedInstanceState?.getParcelable<SearchViewState>(BUNDLE_VIEW_STATE)
        if (previousState != null) {
            render(previousState)
        } else {
            val query = CoordinateQuery(locationToSearch.latitude, locationToSearch.longitude)
            viewModel.weatherBy(query).observe(
                viewLifecycleOwner,
                Observer(::render)
            )
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(BUNDLE_VIEW_STATE, viewModel.cachedData)
    }

    private fun render(viewState: SearchViewState) {
        when (viewState.requestState) {
            RequestState.INITIAL -> {
                searchLoading.remove()
                searchResult.remove()
            }
            RequestState.LOADING -> {
                searchResult.remove()
                searchLoading.show()
            }
            RequestState.SUCCESS -> {
                searchLoading.remove()
                searchResult.show()
                searchResult.bind(viewState.data!!)
            }
            RequestState.ERROR -> {
                searchLoading.remove()
                searchResult.remove()
                context?.shortToast(getString(R.string.something_went_wrong))
            }
        }
    }

    companion object {
        const val BUNDLE_VIEW_STATE = "BUNDLE_VIEW_STATE"
        const val ARGUMENT_LOCATION = "ARGUMENT_LOCATION"

        fun newInstance(location: Location): SearchYourLocationFragment {
            val bundle = Bundle()
            bundle.putParcelable(ARGUMENT_LOCATION, location)
            val fragment = SearchYourLocationFragment()
            fragment.arguments = bundle
            return fragment
        }
    }
}