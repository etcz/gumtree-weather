package au.com.gumtree.weather.search

import android.content.SharedPreferences
import androidx.core.content.edit
import au.com.gumtree.weather.common.RecentSearch
import au.com.gumtree.weather.common.WeatherData
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import javax.inject.Inject

class LastSearchRepository @Inject constructor(
    private val sharedPreferences: SharedPreferences,
    private val gson: Gson
): RecentSearchesRepository {

    private val itemType = object : TypeToken<RecentSearch>() {}.type

    override suspend fun mostRecent(): RecentSearch? {
        val jsonString = sharedPreferences.getString(KEY_LAST_SEARCH, null)
        return if (jsonString != null) gson.fromJson(jsonString, itemType) else null
    }

    override suspend fun save(data: WeatherData, timestamp: Long) {
        sharedPreferences.edit(commit = true) {
            val item = RecentSearch(data, timestamp)
            putString(KEY_LAST_SEARCH, gson.toJson(item))
        }
    }

    companion object {
        private const val KEY_LAST_SEARCH = "KEY_LAST_SEARCH"
    }
}
