package au.com.gumtree.weather.common

import com.google.gson.Gson
import java.io.FileReader

inline fun <reified T> Any.loadTestResource(filename: String): T {
    val resourceFile = this::class.java.classLoader?.getResource(filename)?.file!!
    return Gson().fromJson(FileReader(resourceFile), T::class.java)
}