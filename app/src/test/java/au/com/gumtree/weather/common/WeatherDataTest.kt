package au.com.gumtree.weather.common

import au.com.gumtree.weather.BuildConfig
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class WeatherDataTest {

    private lateinit var apiResponse: WeatherApiResponse

    @Before
    fun setup() {
        apiResponse = loadTestResource("sydney_weather.json")
    }

    @Test
    fun `given an API response, when a weather data is created, then all the fields are correctly formatted`() {
        // when
        val expected = WeatherData(
            location = "Sydney, AU",
            timestamp = "4:41 AM +10:00",
            description = "Light rain",
            iconUrl = "${BuildConfig.WEATHER_ICONS_ENDPOINT}10n@4x.png",
            temperature = "10$DEG_CELSIUS",
            feelsLike = "9$DEG_CELSIUS",
            humidity = "93%",
            windSpeed = "1.5 $METERS_PER_SECOND"
        )

        // then
        assertEquals(expected, WeatherData.from(apiResponse))
    }

    @Test
    fun `given an API response with a malformed icon URL, then the weather data icon URL is empty`() {
        // when
        val fakeWeather = WeatherApiResponse.Weather(
            id = 500,
            main = "Rain",
            description = "light rain",
            icon = "^^"
        )
        val responseWithNoIconUrl = apiResponse.copy(weatherConditions = listOf(fakeWeather))
        val weatherData = WeatherData.from(responseWithNoIconUrl)

        // then
        assertEquals("", weatherData.iconUrl)
    }
}