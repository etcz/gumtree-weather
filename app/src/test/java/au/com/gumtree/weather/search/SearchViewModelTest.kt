package au.com.gumtree.weather.search

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import au.com.gumtree.weather.TestCoroutineRule
import au.com.gumtree.weather.common.*
import com.nhaarman.mockitokotlin2.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers.anyLong
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class SearchViewModelTest {

    @ExperimentalCoroutinesApi
    @get:Rule
    val testCoroutineRule = TestCoroutineRule()
    @get:Rule
    val liveDataRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var weatherRepo: WeatherRepository
    @Mock
    private lateinit var countriesRepo: CountriesRepository
    @Mock
    private lateinit var recentSearchesRepo: RecentSearchesRepository
    private lateinit var searchViewModel: SearchViewModel

    private val sydneyCityQuery = CityQuery(name = "Sydney", countryCode = "AU")

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        searchViewModel = SearchViewModel(weatherRepo, countriesRepo, recentSearchesRepo)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `given a query, when there is no error upstream, the correct sequence is emitted`() {
        testCoroutineRule.runBlockingTest {
            // given
            val apiResponse = loadTestResource<WeatherApiResponse>("sydney_weather.json")
            weatherRepo.stub {
                onBlocking { fetchForecast(any()) }.doReturn(apiResponse)
            }

            // when
            val values = searchViewModel.weatherBy(sydneyCityQuery).awaitValues()

            // then
            assertEquals(RequestState.LOADING, values[0].requestState)
            assertEquals(RequestState.SUCCESS, values[1].requestState)
            assertEquals(WeatherData.from(apiResponse), values[1].data)
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `given a query, when there is an error upstream, the correct sequence is emitted`() {
        testCoroutineRule.runBlockingTest {
            // given
            val upstreamError = RuntimeException("Canceled")
            weatherRepo.stub {
                onBlocking { fetchForecast(any()) }.doThrow(upstreamError)
            }

            // when
            val values = searchViewModel.weatherBy(sydneyCityQuery).awaitValues()

            // then
            assertEquals(RequestState.LOADING, values[0].requestState)
            assertEquals(RequestState.ERROR, values[1].requestState)
            assertEquals(SearchViewState.error(upstreamError), values[1])
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `given a query, when the search is successful, then the data is saved`() {
        testCoroutineRule.runBlockingTest {
            // given
            val apiResponse = loadTestResource<WeatherApiResponse>("sydney_weather.json")
            weatherRepo.stub {
                onBlocking { fetchForecast(any()) }.doReturn(apiResponse)
            }

            // when
            searchViewModel.weatherBy(sydneyCityQuery).awaitValues()

            // then
            verify(recentSearchesRepo, times(1)).save(any(), anyLong())
        }
    }

    private fun LiveData<SearchViewState>.awaitValues(): List<SearchViewState> {
        val returnValues = arrayListOf<SearchViewState>()
        this.observeForever {
            returnValues.add(it)
        }
        return returnValues
    }
}