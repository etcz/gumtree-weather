package au.com.gumtree.weather.common

import org.junit.Assert.assertEquals
import org.junit.Test

class QueryTest {

    @Test
    fun `a query representing a city, returns the expected map`() {
        // when
        val query = CityQuery("Tokyo", "JP")
        val expected = hashMapOf("q" to "Tokyo,jp")

        // then
        assertEquals(expected, query.toMap())
    }

    @Test
    fun `a query representing a zip code, returns the expected map`() {
        // when
        val query = ZipCodeQuery("2000", "AU")
        val expected = hashMapOf("zip" to "2000,au")

        // then
        assertEquals(expected, query.toMap())
    }

    @Test
    fun `a query representing a coordinate (lat, lon), returns the expected map`() {
        // when
        val query = CoordinateQuery(-43.63, 113.34)
        val expected = hashMapOf(
            "lat" to "-43.63",
            "lon" to "113.34"
        )

        // then
        assertEquals(expected, query.toMap())
    }
}