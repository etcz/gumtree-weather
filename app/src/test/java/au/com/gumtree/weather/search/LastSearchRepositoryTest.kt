package au.com.gumtree.weather.search

import au.com.gumtree.weather.FakeSharedPreferences
import au.com.gumtree.weather.common.WeatherApiResponse
import au.com.gumtree.weather.common.WeatherData
import au.com.gumtree.weather.common.loadTestResource
import com.google.gson.Gson
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class LastSearchRepositoryTest {

    private lateinit var lastSearchRepo: LastSearchRepository

    private val fakeSharedPreferences = FakeSharedPreferences()

    @Before
    fun setup() {
        lastSearchRepo = LastSearchRepository(fakeSharedPreferences, Gson())
    }

    @Test
    fun `when new data is saved, then the most recent value is updated`() = runBlocking {
        // given
        val apiResponse = loadTestResource<WeatherApiResponse>("sydney_weather.json")
        val firstItem = WeatherData.from(apiResponse)
        val firstTimestamp = 1L

        val secondApiResponse = apiResponse.copy(timestamp = apiResponse.timestamp + 1)
        val secondItem = WeatherData.from(secondApiResponse)
        val secondTimestamp = 2L

        // when
        lastSearchRepo.save(firstItem, firstTimestamp)

        // then
        var latestSearch = lastSearchRepo.mostRecent()
        assertEquals(firstItem, latestSearch!!.data)
        assertEquals(firstTimestamp, latestSearch.timestamp)

        lastSearchRepo.save(secondItem, secondTimestamp)

        latestSearch = lastSearchRepo.mostRecent()
        assertEquals(secondItem, latestSearch!!.data)
        assertEquals(secondTimestamp, latestSearch.timestamp)
    }
}