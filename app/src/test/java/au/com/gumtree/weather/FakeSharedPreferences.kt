package au.com.gumtree.weather

import android.content.SharedPreferences
import android.content.SharedPreferences.OnSharedPreferenceChangeListener

class FakeSharedPreferences : SharedPreferences {

    private val preferenceMap = mutableMapOf<String, Any?>()
    private val preferenceEditor = MockSharedPreferenceEditor(preferenceMap)

    override fun getAll(): Map<String, *> {
        return preferenceMap
    }

    override fun getString(s: String, s1: String?): String? {
        return preferenceMap[s] as String? ?: s1
    }

    @Suppress("UNCHECKED_CAST")
    override fun getStringSet(s: String, set: Set<String>?): Set<String>? {
        return preferenceMap[s] as Set<String>? ?: set
    }

    override fun getInt(s: String, i: Int): Int {
        return preferenceMap[s] as Int? ?: i
    }

    override fun getLong(s: String, l: Long): Long {
        return preferenceMap[s] as Long? ?: l
    }

    override fun getFloat(s: String, v: Float): Float {
        return preferenceMap[s] as Float? ?: v
    }

    override fun getBoolean(s: String, b: Boolean): Boolean {
        return preferenceMap[s] as Boolean? ?: b
    }

    override fun contains(s: String): Boolean {
        return preferenceMap.containsKey(s)
    }

    override fun edit(): SharedPreferences.Editor {
        return preferenceEditor
    }

    override fun registerOnSharedPreferenceChangeListener(onSharedPreferenceChangeListener: OnSharedPreferenceChangeListener) {
    }

    override fun unregisterOnSharedPreferenceChangeListener(onSharedPreferenceChangeListener: OnSharedPreferenceChangeListener) {
    }

    class MockSharedPreferenceEditor(private val preferenceMap: MutableMap<String, Any?>) : SharedPreferences.Editor {

        override fun putString(s: String, s1: String?): SharedPreferences.Editor {
            preferenceMap[s] = s1
            return this
        }

        override fun putStringSet(s: String, set: Set<String>?): SharedPreferences.Editor {
            preferenceMap[s] = set
            return this
        }

        override fun putInt(s: String, i: Int): SharedPreferences.Editor {
            preferenceMap[s] = i
            return this
        }

        override fun putLong(s: String, l: Long): SharedPreferences.Editor {
            preferenceMap[s] = l
            return this
        }

        override fun putFloat(s: String, v: Float): SharedPreferences.Editor {
            preferenceMap[s] = v
            return this
        }

        override fun putBoolean(s: String, b: Boolean): SharedPreferences.Editor {
            preferenceMap[s] = b
            return this
        }

        override fun remove(s: String): SharedPreferences.Editor {
            preferenceMap.remove(s)
            return this
        }

        override fun clear(): SharedPreferences.Editor {
            preferenceMap.clear()
            return this
        }

        override fun commit(): Boolean {
            return true
        }

        override fun apply() {
            // Nothing to do, everything is saved in memory.
        }
    }
}